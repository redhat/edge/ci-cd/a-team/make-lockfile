# make-lockfile

make-lockfile.py utility to generate lockfiles from content-resolver workloads.

## About

The script make-lockfiles.py can be used to re-create the
cs9-image-manifest.lock.json file in the automotive-sig repo.

Note that this program _only_ generates the lockfile files and does not make
any merge requests to update the git repositories that hold the lockfile files.
This is done in another program named
[update-lockfile](https://gitlab.com/redhat/edge/ci-cd/pipe-x/update-lockfile)
which wraps this make-lockfile program.

## HLA Diagram

The following is an HLA of the project (not reflecting actual internals).

![make-lockfile](docs/make-lockfile.png "make-lockfile")

## Running

To generate the lockfile locally you can build and run the container image with the following commands.

```sh
podman build -t make-lockfile:latest .
podman run make-lockfile:latest
```

If successful, the script will output a lockfile to stdout.

The inputs/outputs are explained in the below sections.

Alternatively, you can execute `run.sh` which has pre-configured inputs.

## Inputs

The script requires two main sources of input:

* tiny-distro-builder (aka content-resolver) environments for list of package
  names
* compose aka RPM repositories to determine the versions of packages

It can also optionally take some koji server parameters, to find packages
built in Koji but not exposed with an RPM repository. To do this, it then requires the
koji server urls and the koji tag associated to search.

### Content-Resolver Input

By default the script uses the cs9 results of tiny distro builder
(<https://tiny.distro.builders/>) as input. The script by default will download
the cs9 environments which contains package definitions for both aarch64 and
x86_64.

Tiny distro builder is fed by two workloads defined here by the BE-team:

(1) <https://tiny.distro.builders/workload-overview--automotive-workload-in--automotive-repositories-c9s.html>

(2) <https://tiny.distro.builders/workload-overview--automotive-workload-off--automotive-repositories-c9s.html>

These are consumed nightly by tiny distro builder and run through
content-resolver (<https://github.com/minimization/content-resolver>) to generate
the results hosted on the internet.

This results in four environment files (two for aarch64, two for x86_64).

(1) <https://tiny.distro.builders/workload--automotive-workload-in--automotive-environment--automotive-repositories-c9s--aarch64.html>

(2) <https://tiny.distro.builders/workload--automotive-workload-in--automotive-environment--automotive-repositories-c9s--x86_64.html>

(3) <https://tiny.distro.builders/workload--automotive-workload-off--automotive-environment--automotive-repositories-c9s--aarch64.html>

(4) <https://tiny.distro.builders/workload--automotive-workload-off--automotive-environment--automotive-repositories-c9s--x86_64.html>

The defaults can be overriden and the script can be fed parameters that redirect it to load the input json files from local files or urls.

```sh
--env [local file or url]
```

Simply add as many files or urls as needed after --env.

```sh
--env [url_1] [url_2]
```

The ability to reference local files is useful when running content-resolver
locally and testing directly the results.

Example:

```sh
wget 'https://tiny.distro.builders/workload--automotive-workload-in--automotive-environment--automotive-repositories-c9s--aarch64.html' -O ./aarch64.json
wget 'https://tiny.distro.builders/workload--automotive-workload-in--automotive-environment--automotive-repositories-c9s--x86_64.html' -O ./x86_64.json
podman run \
  -v $(pwd):/mnt \
  make-lockfile \
  --env /mnt/aarch64.json /mnt/x86_64.json
```

### Compose Input

By default the script uses the latest repositories from the Centos 9 Stream production compose
(<https://composes.stream.centos.org/development/latest-CentOS-Stream/>)
and the CentOS-buildlogs-mirror repositories (<https://buildlogs.centos.org/9-stream/automotive/>
and <https://buildlogs.centos.org/9-stream/autosd/>) as input.

The script by default will download the repodata from the following repositories
and extract the newest versions of the packages available.

(1) <https://buildlogs.centos.org/9-stream/autosd/x86_64/packages-main>

(2) <https://buildlogs.centos.org/9-stream/autosd/aarch64/packages-main>

(3) <https://buildlogs.centos.org/9-stream/automotive/aarch64/packages-main>

(4) <https://buildlogs.centos.org/9-stream/automotive/x86_64/packages-main>

(5) <https://composes.stream.centos.org/production/latest-CentOS-Stream/compose/BaseOS/aarch64/os>

(6) <https://composes.stream.centos.org/production/latest-CentOS-Stream/compose/BaseOS/x86_64/os>

(7) <https://composes.stream.centos.org/production/latest-CentOS-Stream/compose/AppStream/aarch64/os>

(8) <https://composes.stream.centos.org/production/latest-CentOS-Stream/compose/AppStream/x86_64/os>

(9) <https://composes.stream.centos.org/production/latest-CentOS-Stream/compose/CRB/aarch64/os>

(10) <https://composes.stream.centos.org/production/latest-CentOS-Stream/compose/CRB/x86_64/os>

This is then combined with the names from the Content-Resolver input to determine
which packages to put in the lockfile.

The defaults can be overriden and the script can be fed parameters that redirect it
to load versions from a different set of repositories.

```sh
-- repos [repo_urls]
```

Simply add as many urls as needed after --repos.

```sh
-- repos [url_1] [url_2]
```

Example:

```sh
podman run \
  -v $(pwd):/mnt \
  make-lockfile \
  --repos \
  https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/BaseOS/aarch64/os/ \
  https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/BaseOS/x86_64/os/
```

### Koji package resolution

The script can take an optional `--kojiserver` and `--kojitag`, which allows it to parse the koji server and tag
for packages that might not exist in the compose/CentOS-buildlogs-mirror input. This method also allows appending packages that aren't located
in the upstream compose/CentOS-buildlogs-mirror.

Example:

```sh
    -v $(pwd):/mnt \
  make-lockfile \
  --repos \
  https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/BaseOS/aarch64/os/ \
  https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/BaseOS/x86_64/os/ \
  --kojiserver "https://koji.fedoraproject.org/kojihub" \
  --kojitag mytag-1.0.0
```

### Force appended NVRs

The script can optionally force append NVRS to the generated make-lockfile. This is useful in situations where
a developer needs to add a package that isn't resolved by Content Resolver, or to add a specific version of a package for testing.

NOTE: The validity of these NVRs is the users responsibility, and the script does not, and will not check whether these are real packages
or whether they can be reached in any way from any of the provided repositories.

```sh
  -v $(pwd):/mnt \
  make-lockfile \
  --distro cs9 \
  --env \
  https://tiny.distro.builders/workload--automotive-workload-in--automotive-environment--automotive-repositories-c9s--aarch64.json \
  https://tiny.distro.builders/workload--automotive-workload-in--automotive-environment--automotive-repositories-c9s--x86_64.json \
  https://tiny.distro.builders/workload--automotive-workload-off--automotive-environment--automotive-repositories-c9s--aarch64.json \
  https://tiny.distro.builders/workload--automotive-workload-off--automotive-environment--automotive-repositories-c9s--x86_64.json \
  --outfile cs9-image-manifest.lock.json \
  --forcenvrs "my-awesome-nvr-5.14.0-101.62.el9s" "my-awesomer-nvr-5.14.0-101.62.el9s" "kernel-automotive-modules-5.14.0-101.62.el9s"

```

## Outputs

The script outputs directly to stdout the json file resulting from the two input files.

This outputted file can be used as the cs9-image-manifest.json.

The parameter --outfile can be used to redirect the output to a file instead.

```sh
podman run \
  -v $(pwd):/mnt \
  make-lockfile \
  --outfile "/mnt/cs9-image-manifest.lock.json"
```

## Debugging

If the script is run with --debug, the script will output debug messages to stderr.

### Tests

To run the tests locally execute `tests/test.sh`.
